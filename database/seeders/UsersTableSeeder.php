<?php

namespace Database\Seeders;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelRoles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::whereName('Admin')->first();
        $userRole = Role::whereName('User')->first();

        // Seed test admin
        $seededAdminEmail = 'admin@admin.com';
        $admin = User::where('email', '=', $seededAdminEmail)->first();
        if ($admin === null) {
            $admin = User::create([
                'name'                           => 'Admin',
                'email'                          => $seededAdminEmail,
                'password'                       => Hash::make('password'),
                'token'                          => str_random(64),
                'cpf'                            => '620.839.980-73',
                'birthday'                       => \Carbon\Carbon::now()->subYears(30),
                'zip_code'                       => '37548-000',
                'address'                        => 'Rua Pereira',
                'activated'                      => true,
            ]);

            $admin->attachRole($adminRole);
            $admin->save();
        }

        // Seed test user
        $user = User::where('email', '=', 'user@user.com')->first();
        if ($user === null) {
            $user = User::create([
                'name'                           => 'User',
                'email'                          => 'user@user.com',
                'password'                       => Hash::make('password'),
                'token'                          => str_random(64),
                'cpf'                            => '254.806.580-42',
                'birthday'                       => \Carbon\Carbon::now()->subYears(20),
                'entry_date'                     => \Carbon\Carbon::now()->subDays(20),
                'entry_date_time'                => \Carbon\Carbon::now()->subDays(20)->format('H:i:s'),
                'zip_code'                       => '37548-000',
                'address'                        => 'Rua Freitas',
                'activated'                      => true,
                'user_id'                        => $admin->id,
            ]);

            $user->attachRole($userRole);
            $user->save();
        }

    }
}

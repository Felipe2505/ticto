<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\CaptureIpTrait;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\SearchUserRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelRoles\Models\Role;
use Validator;
use Illuminate\Support\Facades\DB;

class UsersManagementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paginationEnabled = config('usersmanagement.enablePagination');
        if ($paginationEnabled) {
            $users = User::paginate(config('usersmanagement.paginateListSize'));
        } else {
            $users = User::all();
        }
        $roles = Role::all();

        return View('usersmanagement.show-users', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        $users = User::whereHas('roles', function($q){
            $q->where('name', 'admin');
        })->get();

        $data = [
            'users'        => $users,
            'roles'       => $roles,
        ];

        return view('usersmanagement.create-user')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        if($input['role'] == 2 && !isset($input['user_id']))
            return back()->with('error', trans('usersmanagement.managerRequired'));

        $input['password'] = Hash::make($input['password']);
        $input['token'] = str_random(64);
        $input['activated'] = 1;

        $user = User::create($input);

        $user->attachRole($request->input('role'));
        $user->save();

        return redirect('users')->with('success', trans('usersmanagement.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('usersmanagement.show-user', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::all();

        $users = User::whereHas('roles', function($q){
            $q->where('name', 'admin');
        })->get();

        foreach ($user->roles as $userRole) {
            $currentRole = $userRole;
            break;
        }

        $data = [
            'user'        => $user,
            'users'        => $users,
            'roles'       => $roles,
            'currentRole' => $currentRole,
        ];

        return view('usersmanagement.edit-user')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param User                     $user
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $input = $request->all();

        if($input['role'] == 2 && !isset($input['user_id']))
            return back()->with('error', trans('usersmanagement.managerRequired'));

        if(isset($input['entry_date'])){
            if(isset(explode('T', $input['entry_date'])[1]))
                $input['entry_date_time'] = explode('T', $input['entry_date'])[1];
            else
                $input['entry_date_time'] = '00.00.00';
                
            $input['entry_date'] = \Carbon\Carbon::parse($input['entry_date'])->format('Y-m-d H:i:s');
        }

        if($input['email'] !== $user->email)
            unset($input['email']);

        if($input['cpf'] !== $user->cpf)
            unset($input['cpf']);

        $userRole = $input['role'];
        if ($userRole !== null) {
            $user->detachAllRoles();
            $user->attachRole($userRole);
        }

        $user->update($input);

        return back()->with('success', trans('usersmanagement.updateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $currentUser = Auth::user();

        if ($user->id !== $currentUser->id) {
            $user->save();
            $user->delete();

            return redirect('users')->with('success', trans('usersmanagement.deleteSuccess'));
        }

        return back()->with('error', trans('usersmanagement.deleteSelfError'));
    }

    /**
     * Method to search the users.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(SearchUserRequest $request)
    {
        $users = DB::select('select * from users where user_id=? and entry_date between ? and ?', [\Auth::user()->id, $request['user_search_box'] . ' 00:00:00', $request['user_search_box_end'] . ' 23:59:59']);
        $users = User::hydrate($users);
        $roles = Role::all();

        $data = [
            'users'               => $users,
            'roles'               => $roles,
            'user_search_box'     => $request['user_search_box'],
            'user_search_box_end' => $request['user_search_box_end']
        ];

        return View('usersmanagement.show-users')->with($data);
    }

}

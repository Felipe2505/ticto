<?php

namespace App\Http\Controllers;

use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->isAdmin()) {
            return view('pages.admin.home');
        }

        return view('pages.user.home');
    }

    public function register()
    {
        $user = Auth::user();

        $today = \Carbon\Carbon::now();
        $user->entry_date = $today;
        $user->entry_date_time = $today->format('H:i:s');
        $user->save();

        return redirect('home');
    }

}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|max:255',
            'cpf'                   => 'required|max:14|unique:users',
            'birthday'              => 'required',
            'zip_code'              => 'required',
            'address'               => 'required',
            'email'                 => 'required|email|max:255|unique:users',
            'password'              => 'required|min:6|max:20|confirmed',
            'password_confirmation' => 'required|same:password',
            'role'                  => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'        => trans('auth.userNameRequired'),
            'cpf.unique'           => trans('auth.cpfTaken'),
            'cpf.required'         => trans('auth.cpfRequired'),
            'birthday.required'    => trans('auth.birthdayRequired'),
            'zip_code.required'    => trans('auth.zipCodeRequired'),
            'address.required'     => trans('auth.addressRequired'),
            'email.required'       => trans('auth.emailRequired'),
            'email.email'          => trans('auth.emailInvalid'),
            'password.required'    => trans('auth.passwordRequired'),
            'password.min'         => trans('auth.PasswordMin'),
            'password.max'         => trans('auth.PasswordMax'),
            'role.required'        => trans('auth.roleRequired'),
        ];
    }
}

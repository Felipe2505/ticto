<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|max:255',
            'cpf'                   => 'required|max:14',
            'birthday'              => 'required',
            'zip_code'              => 'required',
            'address'               => 'required',
            'email'                 => 'required|email|max:255',
            'role'                  => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'        => trans('auth.userNameRequired'),
            'cpf.required'         => trans('auth.cpfRequired'),
            'birthday.required'    => trans('auth.birthdayRequired'),
            'zip_code.required'    => trans('auth.zipCodeRequired'),
            'address.required'     => trans('auth.addressRequired'),
            'email.required'       => trans('auth.emailRequired'),
            'email.email'          => trans('auth.emailInvalid'),
            'password.max'         => trans('auth.PasswordMax'),
            'role.required'        => trans('auth.roleRequired'),
        ];
    }
}

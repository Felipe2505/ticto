<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasRoleAndPermission;
    use Notifiable;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that are hidden.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'activated',
        'token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'birthday',
        'entry_date',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'activated',
        'token',
        'cpf',
        'birthday',
        'entry_date',
        'entry_date_time',
        'zip_code',
        'address',
        'user_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                                => 'integer',
        'user_id'                           => 'integer',
        'email'                             => 'string',
        'password'                          => 'string',
        'activated'                         => 'boolean',
        'token'                             => 'string',
        'cpf'                               => 'string',
        'birthday'                          => 'date',
        'entry_date'                        => 'date',
        'zip_code'                          => 'string',
        'address'                           => 'string',
    ];

    public function getReadableEntryDateAttribute()
    {
        return is_null($this->entry_date) ? null : Carbon::parse($this->entry_date)->format('d/m/Y | ') . $this->entry_date_time;
    }

    public function getReadableCreatedAtAttribute()
    {
        return is_null($this->created_at) ? null : Carbon::parse($this->created_at)->format('d/m/Y | H:i');
    }

    public function getReadableUpdatedAtAttribute()
    {
        return is_null($this->updated_at) ? null : Carbon::parse($this->updated_at)->format('d/m/Y | H:i');
    }

    public function getReadableBirthdayAttribute()
    {
        return is_null($this->birthday) ? null : Carbon::parse($this->birthday)->format('d/m/Y');
    }

    public function getReadableAgeAttribute()
    {
        return is_null($this->birthday) ? null : Carbon::now()->diffInYears($this->birthday);
    }

    /**
     * Get the socials for the user.
     */
    public function manager()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

}

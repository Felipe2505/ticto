<?php

    $levelAmount = 'level';

    if (Auth::User()->level() >= 2) {
        $levelAmount = 'levels';

    }

?>

<div class="card">
    <div class="card-header <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?> bg-secondary text-white <?php endif; ?>">

        Bem vindo <?php echo e(Auth::user()->name); ?>


        <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?>
            <span class="pull-right badge badge-primary" style="margin-top:4px">
                Admin Access
            </span>
        <?php else: ?>
            <span class="pull-right badge badge-warning" style="margin-top:4px">
                User Access
            </span>
        <?php endif; ?>

    </div>
    <div class="card-body">

        <?php if (Auth::check() && Auth::user()->hasRole('admin', true)): ?>
            <h2>Dashboard</h2>
        <?php endif; ?>

        <?php if (Auth::check() && Auth::user()->hasRole('user', true)): ?>
            <?php if(\Auth::user()->entry_date != null): ?>
                <div class="col-sm-3">
                    <strong>Você já registrou o ponto</strong>
                </div>
            <?php else: ?>
                <div class="col-sm-3">
                    <a class="btn btn-sm btn-info btn-block" href="<?php echo e(URL::to('register-entry')); ?>" data-toggle="tooltip" title="Registrar">
                        Registrar ponto
                    </a>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    </div>
</div>
<?php /**PATH /home/bruno/Documentos/final/laravel-auth/resources/views/panels/welcome-panel.blade.php ENDPATH**/ ?>
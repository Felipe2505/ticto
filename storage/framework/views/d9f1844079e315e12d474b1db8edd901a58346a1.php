<div class="row">
    <div class="col-sm-12 offset-sm-6 col-md-6 offset-md-6 col-lg-5 offset-lg-7 col-xl-4 offset-xl-8">
        <?php echo Form::open(['route' => 'search-users', 'method' => 'POST', 'role' => 'form']); ?>

            <?php echo csrf_field(); ?>

            <div class="row">
                <div class="form-group row">
                    <div class="">
                        <?php echo Form::label('user_search_box', trans('usersmanagement.labelSearchBegin'), array('class' => 'control-label'));; ?>

                        <div class="input-group">
                            <?php echo Form::date('user_search_box', isset($user_search_box) ? $user_search_box : null, array('id' => 'user_search_box', 'class' => 'form-control', 'max' => date('Y-m-d'))); ?>

                        </div>
                    </div>
                    <div class="">
                        <?php echo Form::label('user_search_box_end', trans('usersmanagement.labelSearchEnd'), array('class' => 'control-label'));; ?>

                        <div class="input-group">
                            <?php echo Form::date('user_search_box_end', isset($user_search_box_end) ? $user_search_box_end : null, array('id' => 'user_search_box_end', 'class' => 'form-control', 'max' => date('Y-m-d'))); ?>

                            <div class="input-group-append">
                                <a href="#" class="input-group-addon btn btn-warning clear-search" data-toggle="tooltip" title="<?php echo e(trans('usersmanagement.tooltips.clear-search')); ?>" style="display:none;">
                                    <i class="fa fa-fw fa-times" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        <?php echo trans('usersmanagement.tooltips.clear-search'); ?>

                                    </span>
                                </a>
                                <button class="input-group-addon btn btn-secondary" type="submit" data-toggle="tooltip" data-placement="bottom" title="<?php echo e(trans('usersmanagement.tooltips.submit-search') . \Auth::user()->name); ?>" >
                                    <i class="fa fa-search fa-fw" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        <?php echo trans('usersmanagement.tooltips.submit-search'); ?>

                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        <?php echo Form::close(); ?>

    </div>
</div>

<?php /**PATH /home/bruno/Documentos/laravel-auth/resources/views/partials/search-users-form.blade.php ENDPATH**/ ?>
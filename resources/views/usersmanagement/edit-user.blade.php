@extends('layouts.app')

@section('template_title')
    {!! trans('usersmanagement.editing-user', ['name' => $user->name]) !!}
@endsection

@section('template_linked_css')
    {{-- <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style> --}}
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            {!! trans('usersmanagement.editing-user', ['name' => $user->name]) !!}
                            <div class="pull-right">
                                <a href="{{ route('users') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="top" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-users') !!}
                                </a>
                                <a href="{{ url('/users/' . $user->id) }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-reply" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-user') !!}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => ['users.update', $user->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'needs-validation')) !!}

                            {!! csrf_field() !!}

                            <div class="form-group has-feedback row {{ $errors->has('name') ? ' has-error ' : '' }}">
                                {!! Form::label('name', trans('forms.create_user_label_username'), array('class' => 'col-md-3 control-label')); !!}
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('name', $user->name, array('id' => 'name', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_username'))) !!}
                                        <div class="input-group-append">
                                            <label class="input-group-text" for="name">
                                                <i class="fa fa-fw {{ trans('forms.create_user_icon_username') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-md-3 control-label')); !!}
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                                        <div class="input-group-append">
                                            <label for="email" class="input-group-text">
                                                <i class="fa fa-fw {{ trans('forms.create_user_icon_email') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('cpf') ? ' has-error ' : '' }}">
                                {!! Form::label('cpf', trans('forms.create_user_label_cpf'), array('class' => 'col-md-3 control-label')); !!}
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('cpf', $user->cpf, array('id' => 'cpf', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_cpf'))) !!}
                                        <div class="input-group-append">
                                            <label for="cpf" class="input-group-text">
                                                <i class="fa fa-fw {{ trans('forms.create_user_icon_cpf') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    <span id="cpf_status"></span>
                                    @if ($errors->has('cpf'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('cpf') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('role') ? ' has-error ' : '' }}">

                                {!! Form::label('role', trans('forms.create_user_label_role'), array('class' => 'col-md-3 control-label')); !!}

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="role" id="role">
                                            <option value="">{{ trans('forms.create_user_ph_role') }}</option>
                                            @if ($roles)
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}" {{ $currentRole->id == $role->id ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text" for="role">
                                                <i class="{{ trans('forms.create_user_icon_role') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('role'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('birthday') ? ' has-error ' : '' }}">
                                {!! Form::label('birthday', trans('forms.create_user_label_birthday'), array('class' => 'col-md-3 control-label')); !!}
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::date('birthday', $user->birthday, array('id' => 'birthday', 'class' => 'form-control', 'max' => date('Y-m-d'), 'placeholder' => trans('forms.create_user_ph_birthday'))) !!}
                                        <div class="input-group-append">
                                            <label for="birthday" class="input-group-text">
                                                <i class="fa fa-fw {{ trans('forms.create_user_icon_birthday') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('birthday'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('birthday') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('entry_date') ? ' has-error ' : '' }}" id="div_entry_date">
                                {{-- {!! Form::label('entry_date', trans('forms.create_user_label_entry_date'), array('class' => 'col-md-3 control-label', 'max' => \Carbon\Carbon::parse(date('Y-m-d'))->format('Y-m-d\T00:00:00'))); !!} --}}
                                {!! Form::label('entry_date', trans('forms.create_user_label_entry_date'), array('class' => 'col-md-3 control-label')); !!}
                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input type="datetime-local" id="entry_date" name="entry_date" class="form-control" max= "{{\Carbon\Carbon::parse(date('Y-m-d'))->format('Y-m-d\T00:00:00')}}" value="{{\Carbon\Carbon::parse($user->entry_date)->format('Y-m-d\T') . $user->entry_date_time}}">
                                        {{-- <input type="datetime-local" id="entry_date" name="entry_date" class="form-control" value="2000-01-01T03:50:00"> --}}
                                        <div class="input-group-append">
                                            <label for="entry_date" class="input-group-text">
                                                <i class="fa fa-fw {{ trans('forms.create_user_icon_entry_date') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('entry_date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('entry_date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('zip_code') ? ' has-error ' : '' }}">
                                {!! Form::label('zip_code', trans('forms.create_user_label_zip_code'), array('class' => 'col-md-3 control-label')); !!}
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('zip_code', $user->zip_code, array('id' => 'zip_code', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_zip_code'))) !!}
                                        <div class="input-group-append">
                                            <label for="zip_code" class="input-group-text">
                                                <i class="fa fa-fw {{ trans('forms.create_user_icon_zip_code') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('zip_code'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('address') ? ' has-error ' : '' }}">
                                {!! Form::label('address', trans('forms.create_user_label_address'), array('class' => 'col-md-3 control-label')); !!}
                                <div class="col-md-9">
                                    <div class="input-group">
                                        {!! Form::text('address', $user->address, array('id' => 'address', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_address'))) !!}
                                        <div class="input-group-append">
                                            <label for="address" class="input-group-text">
                                                <i class="fa fa-fw {{ trans('forms.create_user_icon_address') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group has-feedback row {{ $errors->has('user_id') ? ' has-error ' : '' }}" id="div_manager">

                                {!! Form::label('user_id', trans('forms.create_user_manager'), array('class' => 'col-md-3 control-label')); !!}

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="user_id" id="user_id">
                                            <option value="">{{ trans('forms.create_user_ph_manager') }}</option>
                                                @foreach($users as $u)
                                                    <option value="{{ $u->id }}" {{ $u->id == $user->user_id ? 'selected="selected"' : '' }}>{{ $u->name }}</option>
                                                @endforeach
                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text" for="user_id">
                                                <i class="{{ trans('forms.create_user_icon_user_id') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('user_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12 col-sm-6 mb-2">
                                </div>
                                <div class="col-12 col-sm-6">
                                    {!! Form::button(trans('forms.save-changes'), array('class' => 'btn btn-success btn-block margin-bottom-1 mt-3 mb-2 btn-save','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmSave', 'data-title' => trans('modals.edit_user__modal_text_confirm_title'), 'data-message' => trans('modals.edit_user__modal_text_confirm_message'), 'id' => 'save_trigger')) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-save')
    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  {{-- @include('scripts.check-changed') --}}

  <script src="{{asset('js/jquery.mask.min.js')}}"></script>
    <script type="text/javascript">

        @if($user->isAdmin())
            $("#div_manager").hide();
        @endif

        $("#cpf").mask('999.999.999-99', {reverse: false});

        $("#cpf").keyup(function() {
            if(!checkCpfIsValid($(this).val())){
                $("#cpf_status").html("CPF inválido!");
                $("#save_trigger").attr('disabled', true);
            }
            else{
                $("#cpf_status").html("");
                $("#save_trigger").attr('disabled', false);
            }
        });

        $("#zip_code").mask("99999-999");

        $("#zip_code").blur(function(){
            searchAddress($(this).val());
        });

        if($("#role option:selected").text() == "Admin"){
            $("#div_entry_date").hide();
        }

        $("#role").change(function(){
            if($("#role option:selected").text() == "Admin"){
                $("#div_manager").hide();
                $("#div_entry_date").hide();
            }
            else{
                $("#div_manager").show();
                $("#div_entry_date").show();
            }
        });

        function checkCpfIsValid(cpf) {
            cpf = cpf.replace(/[^\d]+/g, '');
            if (cpf == '') return false;
            // Elimina CPFs invalidos conhecidos    
            if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999") return false;
            // Valida 1o digito 
            add = 0;
            for (i = 0; i < 9; i++) add += parseInt(cpf.charAt(i)) * (10 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11) rev = 0;
            if (rev != parseInt(cpf.charAt(9))) return false;
            // Valida 2o digito 
            add = 0;
            for (i = 0; i < 10; i++) add += parseInt(cpf.charAt(i)) * (11 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11) rev = 0;
            if (rev != parseInt(cpf.charAt(10))) return false;
            return true;
        }

        function searchAddress(zip_code){
            var cep = zip_code.replace(/[^0-9]/, '');
            var address = '';
            if(cep){
                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                    if (!("erro" in dados)) {
                        console.log(dados)
                        //Atualiza os campos com os valores da consulta.
                        if(dados.logradouro != ''){
                            address = dados.logradouro;
                        }
                        if(dados.bairro != ''){
                            address = address + ' ' + dados.bairro;
                        }
                        if(dados.localidade != ''){
                            address = address + ' ' + dados.localidade;
                        }
                        if(dados.uf != ''){
                            address = address + ' ' + dados.uf;
                        }

                        $("#address").val(address);
                    } //end if.
                    else {
                        // alert("CEP não encontrado.");
                    }
                });
            }
        }					

    </script>
@endsection

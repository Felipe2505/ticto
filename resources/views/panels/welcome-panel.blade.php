@php

    $levelAmount = 'level';

    if (Auth::User()->level() >= 2) {
        $levelAmount = 'levels';

    }

@endphp

<div class="card">
    <div class="card-header @role('admin', true) bg-secondary text-white @endrole">

        Bem vindo {{ Auth::user()->name }}

        @role('admin', true)
            <span class="pull-right badge badge-primary" style="margin-top:4px">
                Admin Access
            </span>
        @else
            <span class="pull-right badge badge-warning" style="margin-top:4px">
                User Access
            </span>
        @endrole

    </div>
    <div class="card-body">

        @role('admin', true)
            <h2>Dashboard</h2>
        @endrole

        @role('user', true)
            @if(\Auth::user()->entry_date != null)
                <div class="col-sm-3">
                    <strong>Você já registrou o ponto</strong>
                </div>
            @else
                <div class="col-sm-3">
                    <a class="btn btn-sm btn-info btn-block" href="{{ URL::to('register-entry') }}" data-toggle="tooltip" title="Registrar">
                        Registrar ponto
                    </a>
                </div>
            @endif
        @endrole

    </div>
</div>

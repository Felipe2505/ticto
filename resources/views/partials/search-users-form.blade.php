<div class="row">
    <div class="col-sm-12 offset-sm-6 col-md-6 offset-md-6 col-lg-5 offset-lg-7 col-xl-4 offset-xl-8">
        {!! Form::open(['route' => 'search-users', 'method' => 'POST', 'role' => 'form']) !!}
            {!! csrf_field() !!}
            <div class="row">
                <div class="form-group row">
                    <div class="">
                        {!! Form::label('user_search_box', trans('usersmanagement.labelSearchBegin'), array('class' => 'control-label')); !!}
                        <div class="input-group">
                            {!! Form::date('user_search_box', isset($user_search_box) ? $user_search_box : null, array('id' => 'user_search_box', 'class' => 'form-control', 'max' => date('Y-m-d'))) !!}
                        </div>
                    </div>
                    <div class="">
                        {!! Form::label('user_search_box_end', trans('usersmanagement.labelSearchEnd'), array('class' => 'control-label')); !!}
                        <div class="input-group">
                            {!! Form::date('user_search_box_end', isset($user_search_box_end) ? $user_search_box_end : null, array('id' => 'user_search_box_end', 'class' => 'form-control', 'max' => date('Y-m-d'))) !!}
                            <div class="input-group-append">
                                <a href="#" class="input-group-addon btn btn-warning clear-search" data-toggle="tooltip" title="{{ trans('usersmanagement.tooltips.clear-search') }}" style="display:none;">
                                    <i class="fa fa-fw fa-times" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        {!! trans('usersmanagement.tooltips.clear-search') !!}
                                    </span>
                                </a>
                                <button class="input-group-addon btn btn-secondary" type="submit" data-toggle="tooltip" data-placement="bottom" title="{{ trans('usersmanagement.tooltips.submit-search') . \Auth::user()->name}}" >
                                    <i class="fa fa-search fa-fw" aria-hidden="true"></i>
                                    <span class="sr-only">
                                        {!!  trans('usersmanagement.tooltips.submit-search') !!}
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        {!! Form::close() !!}
    </div>
</div>


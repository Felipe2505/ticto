<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Profiles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match user profile
    | pages, messages, forms, labels, placeholders, links, and buttons.
    |
    */

    'templateTitle' => 'Editar seu perfil',

    // profile erros
    'notYourProfile'      => 'Este não é o perfil que está procurando!',
    'notYourProfileTitle' => 'Hmmm, algo deu errrado',
    'noProfileYet'        => 'Ainda não existe um perfil.',

    // USER profile title
    'showProfileUsername'        => 'Nome',
    'showProfileFirstName'       => 'Nome',
    'showProfileLastName'        => 'Sobrenome',
    'showProfileEmail'           => 'Endereço de Email',
    'showProfileLocation'        => 'Localização',
    'showProfileBio'             => 'Biografia',
    'showProfileTheme'           => 'Tema',

    // USER profile page
    'showProfileTitle' => ':username\'s Perfil',

    // USER EDIT profile page
    'editProfileTitle' => 'Configuração do perfil',

    // User edit profile form
    'label-theme' => 'Seu tema:',
    'ph-theme'    => 'Selecione o seu tema',

    'label-location' => 'Sua localização:',
    'ph-location'    => 'Digite sua localização',

    'label-bio' => 'Biografia:',
    'ph-bio'    => 'Digite sua biografia',

    // User Account Settings Tab
    'editTriggerAlt'        => 'Toggle User Menu',
    'editAccountTitle'      => 'Configuração da Conta',
    'editAccountAdminTitle' => 'Configuração da Conta de  Administrador',
    'updateAccountSuccess'  => 'Sua conta foi atualizada com sucesso!',
    'submitProfileButton'   => 'Salvar mudanças',

    // User Account Admin Tab
    'submitPWButton'    => 'Atualizar Senha',
    'changePwTitle'     => 'Mudar de Senha',
    'changePwPill'      => 'Mudar de Senha',
    'deleteAccountPill' => 'Apagar conta',
    'updatePWSuccess'   => 'Sua senha foi atualizada com sucesso!',

    // Delete Account Tab
    'deleteAccountTitle'        => 'Conta Deletada com sucesso',
    'deleteAccountBtn'          => 'Apagar minha conta',
    'deleteAccountBtnConfirm'   => 'Apagar minha conta',
    'deleteAccountConfirmTitle' => 'Confirmar deletação de sua conta',
    'deleteAccountConfirmMsg'   => 'Tem certeza que deseja apagar sua conta?',
    'confirmDeleteRequired'     => 'Confirme a conta a ser apagada pois é obrigatório',

    'errorDeleteNotYour'        => 'Você pode apagar somente o seu perfil',
    'successUserAccountDeleted' => 'Sua conta foi deletada com sucesso!',

    // Messages
    'updateSuccess' => 'As informações do seu perfil foram atualizadas!',
    'submitButton'  => 'Salvar mudanças',

    // Restore User Account
    'errorRestoreUserTime' => 'Desculpe, sua conta não pode ser recuperada',
    'successUserRestore'   => 'Bem vindo de volta :username! Sua conta foi recuperada com sucesso',

];
